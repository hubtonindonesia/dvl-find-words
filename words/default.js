define({
  options: {
    // these options override the ones in app.js
    alphabet: 'abcdefghijklmnopqrstuvwxyz',
    size: 15,
    hint: true,
    showDescriptions: true,
    totalWords: 10,
    initialScore: 1000,
    every: 10
  },

  data: {
    // format:
    //      word-in-list: description
    Satu: '',
    Dua: '',
    Tiga: '',
    Empat: '',
    Lima: '',
    Enam: '',
    Tujuh: '',
    Delapan: '',
    Sembilan: '',
    Sepuluh: ''
  }
});
