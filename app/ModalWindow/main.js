define(['./Utils', 'css!./modal.css'], function ($) {
  'use strict';

  var $modal, $msg, $close, $overlay, currentContent;

  function initialize() {
    $modal = $.$('modal-window');
    $msg = $.$('modal-window-msg');
    $close = $.$('modal-window-close');
    $overlay = $.$('modal-window-overlay');

    $.on($close, 'click', closeModal);
  }

  function replace(text, tmpl) {
    var i;

    for (i in tmpl) {
      if (tmpl.hasOwnProperty(i)) {
        text = text.replace(new RegExp('{' + i + '}', 'gi'), tmpl[i]);
      }
    }

    return text;
  }

  function showModal(id, tmpl) {
    var style = $modal.style,
      elem = $.$(id);

    elem.className = '';

    if (elem.nodeName == 'SCRIPT' || tmpl) {
      $msg.innerHTML = replace(elem.innerHTML, tmpl);
      currentContent = null;
    } else {
      elem.style.display = 'block';
      currentContent = elem;
      $msg.appendChild(elem);
    }

    // Centrar Ventana
    var width = $modal.offsetWidth;

    // style.marginLeft = -width / 2 + 'px';
    style.transform = 'translateX(-50%)';
    style.maxWidth = '100%';

    // Mostrar
    $modal.className = 'modal ' + id;
    $overlay.className = '';

    /* @MOD override score form */
    const form = document.getElementById('save-score-form');
    console.log(form);
    form.onsubmit = function (e) {
      e.preventDefault();
      e.stopPropagation();
      console.log(e);
      const data = Object.fromEntries(new FormData(form).entries());
      console.log({ data });

      var searchQuery = searchToObject();
      var gameId = searchQuery.gameId;
      var token = searchQuery.token;

      var BASE_ENDPOINT = "https://dvl2022-api.secure-staging.com/api";
      var url = BASE_ENDPOINT+'/games/'+gameId+'/submit-score';
      var formData = new FormData();
      var parsed = parseInt(data.score);
      formData.append('score', parsed < 0 ? 0 : parsed);

      fetch(url, {
        method: 'post',
        body: formData, // JSON.stringify({score: parseInt(data.score)})
        headers: {
          Authorization: 'Bearer '+token
        }
      })
      .then(function(res){return res.json()})
      .then(function(res){
        if(res.message){
          alert(res.message);
        } else {
          alert("Score successfully saved.");
        }
      })
      .catch(function(err){
        alert("Failed to save.");
      })
    };
    /* MOD end */
  }

  /* @MOD url parameters to object */
  function searchToObject() {
    var pairs = window.location.search.substring(1).split("&"),
      obj = {},
      pair,
      i;

    for ( i in pairs ) {
      if ( pairs[i] === "" ) continue;

      pair = pairs[i].split("=");
      obj[ decodeURIComponent( pair[0] ) ] = decodeURIComponent( pair[1] );
    }

    return obj;
  }

  function closeModal(e) {
    e && e.preventDefault();
    $modal.className = 'modal hide';
    $overlay.className = 'hide';

    var current = currentContent;
    setTimeout(function () {
      if (!current) {
        $msg.innerHTML = '';
        return;
      }
      current.style.display = 'none';
      document.body.appendChild(current);
    }, 600);

    return false;
  }

  return {
    init: initialize,
    open: showModal,
    close: closeModal
  };
});
