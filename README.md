- WARNING: this is a modded game. Using built-in `./admin` feature may break something. Please proceed carefully.
- Search for modified parts: VSCode `ctrl`+`shift`+`f`, use "@MOD" keyword.

## Modifications

- Ganti waktnya jadi mundur dari 60s
- Ada 10 kata yang harus dicari (Adjustable by default)
- Send score ke external URL (API)
- Adjust score ada di `app.js` -> `Soup.points`
